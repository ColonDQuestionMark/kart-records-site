<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
	/**
	 * Run the migrations.
	 */
	public function up()
	{
		Schema::create('courses', function (Blueprint $table) {
			$table->increments('id');
			
			$table->string('name');
			
			$table->timestamps();
		});
		
		Schema::table('maps', function (Blueprint $table) {
			$table->integer('course_id')->unsigned()->nullable();
			
			$table->foreign(['course_id'])
				->references('id')->on('courses')
				->onUpdate('cascade')->onDelete('set null');
		});
	}

	/**
	 * Reverse the migrations.
	 */
	public function down()
	{
		Schema::table('maps', function (Blueprint $table) {
			$table->dropForeign(['course_id']);
			$table->dropColumn('course_id');
		});

		Schema::dropIfExists('courses');
	}
}
