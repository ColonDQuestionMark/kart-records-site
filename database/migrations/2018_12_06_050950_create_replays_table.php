<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReplaysTable extends Migration
{
	/**
	 * Run the migrations.
	 */
	public function up()
	{
		Schema::create('replays', function (Blueprint $table) {
			$table->increments('id');
			
			$table->string('name');
			$table->string('color');
			$table->integer('race_tics');
			$table->integer('lap_tics');
			$table->string('checksum');
			$table->enum('status', ['Verified', 'Major Skips', 'Hacked', 'Hidden'])->nullable();
			
			$table->integer('map_id')->unsigned();
			$table->foreign(['map_id'])
				->references('id')->on('maps')
				->onUpdate('cascade')->onDelete('restrict');
			
			$table->integer('character_id')->unsigned();
			$table->foreign(['character_id'])
				->references('id')->on('characters')
				->onUpdate('cascade')->onDelete('restrict');
			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 */
	public function down()
	{
		Schema::dropIfExists('replays');
	}
}
