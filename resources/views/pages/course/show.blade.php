@extends('layouts.front')

@section('content')
<h1>{{ $course->name }}</h1>

@if($skips)
	<i>Showing major skips</i>
@else
	<a href="{{ action('CourseController@show', ['course' => $course, 'skips' => true]) }}">Show Major Skips</a>
@endif

<h2>Course Times</h2>
@include('partials.leaderboard', ['leaderboard' => $course->time_leaderboard, 'time' => 'race_time'])

<h2>Lap Times</h2>
@include('partials.leaderboard', ['leaderboard' => $course->lap_leaderboard, 'time' => 'lap_time'])
@stop
