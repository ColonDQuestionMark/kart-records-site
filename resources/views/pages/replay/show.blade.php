@extends('layouts.front')

@section('content')
<h1>View Replay</h1>

@if($replay->course)
	<p>On {{ $replay->course->name }}</p>
@else
	<p>On an unidentified course</p>
@endif

<p>Racer: {{ $replay->name }}</p>
<p>Character: {{ $replay->character->name ?: $replay->character->skin }}</p>
<p>Time: {{ $replay->race_time }} ({{ $replay->race_rank }})</p>
<p>Best Lap: {{ $replay->lap_time }} ({{ $replay->lap_rank }})</p>

<a href="{{ action('ReplayController@download', compact('replay')) }}">Download</a>
@stop
