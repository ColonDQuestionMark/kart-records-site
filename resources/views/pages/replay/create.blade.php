@extends('layouts.front')

@section('content')
<h1>Upload Replay</h1>

<form action="{{ action('ReplayController@store') }}" method="POST" enctype="multipart/form-data">
	@csrf
	<label>
		Upload replay:
		<input type="file" name="replay" />
	</label>

	<input type="submit" value="Upload" />
</form>

<p id="replay_drop" ondrop="uploadReplaysHandler(event)" ondragover="event.preventDefault()">Or drag and drop replays here</div>

<script>
	function uploadReplaysHandler(event) {
		event.preventDefault();
		
		if (event.dataTransfer.items) {
			// Use DataTransferItemList interface to access the file(s)
			for (let i = 0; i < event.dataTransfer.items.length; i++) {
				// If dropped items aren't files, reject them
				if (event.dataTransfer.items[i].kind === 'file') {
					uploadReplay(event.dataTransfer.items[i].getAsFile());
				}
			}
		} else {
			// Use DataTransfer interface to access the file(s)
			for (let i = 0; i < event.dataTransfer.files.length; i++) {
				uploadReplay(event.dataTransfer.files[i]);
			}
		} 

		// Pass event to removeDragData for cleanup
		removeDragData(event)
	}
	
	function uploadReplay(file) {
		let url = {!! json_encode(action('ReplayController@store')) !!};
		let formData = new FormData();

		formData.append('_token', {!! json_encode(csrf_token()) !!});
		formData.append('replay', file);

		fetch(url, {
			method: 'POST',
			body: formData,
			credentials: 'include'
		})
		.then(function(response) {
			let result = 'Success';
			
			if (response.status == 422) {
				result = 'Failed';
			} else if (response.status == 409) {
				result = 'Already Exists';
			}
			
			showResult(file.name, result);
		})
		.catch(function() {
			showResult(file.name, 'Failed');
		});
	}
	
	function showResult(filename, result) {
		let el = document.createElement('p');
		el.innerText = filename + ': ';
		el.innerHTML += '<b>' + result + '</b>';
		
		document.getElementById('replay_drop').innerHTML += el.outerHTML;
	}
</script>
@stop
