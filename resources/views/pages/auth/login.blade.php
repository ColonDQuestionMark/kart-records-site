@extends('layouts.front')

@section('content')
<h1>Login</h1>

<form method="POST" action="{{ route('login') }}">
	@csrf
	
	<label>
		Username:
		<input type="text" name="username" value="{{ old('username') }}" />
		@if ($errors->has('username'))
			<strong>{{ $errors->first('username') }}</strong>
		@endif
	</label>
	
	<label>
		Password:
		<input type="password" name="password" value="" />
		@if ($errors->has('password'))
			<strong>{{ $errors->first('password') }}</strong>
		@endif
	</label>
	
	<label>
		<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} />
		Remember me
	</label>
	
	<input type="submit" value="Login" />
</form>
@stop
