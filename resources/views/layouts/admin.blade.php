<!doctype html>
<html id="admin">
	<head>
		<title>@yield('title', 'Kart Records Admin')</title>
		<link href="/style.css" rel="stylesheet" />
	</head>

	<body>
		<div id="header">
			<div id="sitename">Kart Records Admin</div>
			<nav>
				<ul>
					<li><a href="/">Main Site</a></li>
					<li><a href="{{ action('Admin\CourseController@index') }}">Courses</a></li>
					<li><a href="{{ action('Admin\ReplayController@index') }}">Replays</a></li>
				</ul>
			</nav>
		</div>

		<div id="content">
			@yield('content')
		</div>
	</body>
</html>
