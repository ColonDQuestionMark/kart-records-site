REM kart_folder is the folder of your SRB2Kart installation.
REM replay_filename is the filename you want the replay to be temporarily stored as, and can be changed to prevent conflicts.
REM play_command is playdemo for regular speed or timedemo for fast-forward.
REM Set the below variables to what you need and tell your browser to use this script to open srb2kartreplay: URLs.

set kart_folder=C:\SRB2\kart
set replay_filename=replay.lmp
set play_command=playdemo

cd /D "%kart_folder%"
set replay="%1"
PowerShell -Command "iwr -outf \"%replay_filename%\" https://kart.tinted.red/replay/%replay:~18,-1%/download"
srb2kart.exe -%play_command% "%replay_filename%"