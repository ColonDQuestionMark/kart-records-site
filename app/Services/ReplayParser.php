<?php namespace App\Services;

use App\Exceptions\{ReplayDuplicationException, ReplayException};
use App\Models\{Character, Map, Replay};

class ReplayParser
{
	/**
	 * Parses the given replay data into a Replay model. If the replay references a map or character that we don't
	 * know about yet, those models will be created and stored. The replay itself will also be stored.
	 *
	 * @param  string          $replayContents The contents of the replay file to parse.
	 * @throws ReplayException if the replay is invalid for any reason.
	 * @return Replay          The saved replay.
	 */
	public function storeReplay(string $replayContents) : Replay
	{
		$checksum = $this->ensureUnique($replayContents);
		$info = $this->parseInfo($replayContents);
		$this->validateStructure($info['header'], $info['playstring'], $info['demoflags']);
		$this->validateVersion($info['version'], $info['subversion'], $info['demoversion']);
		$this->validateTimes($info['racetime'], $info['laptime']);
		
		$map = Map::firstOrCreate([
			'checksum'   => bin2hex($info['mapmd5']),
			'map_number' => $info['gamemap'],
		]);
		$character = Character::firstOrCreate([
			'skin'   => $info['skin'],
			'speed'  => $info['kartspeed'],
			'weight' => $info['kartweight'],
		]);
		
		$replay = new Replay([
			'race_tics' => $info['racetime'],
			'lap_tics'  => $info['laptime'],
			'name'      => $info['name'],
			'color'     => $info['color'],
			'checksum'  => $checksum,
		]);
		$replay->map()->associate($map);
		$replay->character()->associate($character);
		$replay->save();
		
		$replay->storeFile($replayContents);
		
		return $replay;
	}
	
	protected function ensureUnique(string $replayContents) : string
	{
		$checksum = md5($replayContents);
		
		if (Replay::where(['checksum' => $checksum])->exists()) {
			throw new ReplayDuplicationException('Replay already exists');
		}
		
		return $checksum;
	}
	
	protected function parseInfo(string $replayContents) : array
	{
		return unpack($this->replayFormat(), $replayContents);
	}
	
	protected function validateStructure(string $header, string $playString, int $demoFlags) : void
	{
		if ($header !== "\xF0" . 'KartReplay' . "\x0F") {
			throw new ReplayException('The replay header is not valid');
		}
		
		if ($playString !== 'PLAY') {
			throw new ReplayException('The replay is missing a "PLAY" marker');
		}
		
		if (($demoFlags & 6) !== 2) {
			throw new ReplayException('The replay was not recorded in Record Attack');
		}
	}
	
	protected function validateVersion(int $version, int $subVersion, int $demoVersion) : void
	{
		$cfg = config('app.replay_version');
		
		if ($version != $cfg['major'] || $demoVersion != $cfg['demo']) {
			throw new ReplayException('This replay is not supported by the records site');
		}
		
		if ($subVersion < $cfg['sub']) {
			throw new ReplayException('This replay was recorded on too old of a version');
		}
	}
	
	protected function validateTimes(int $race, int $lap) : void
	{
		if ($race < 35 || $lap < 35) {
			throw new ReplayException('This replay appears too short to be valid');
		}
		
		if ($race > 35*60*60 || $lap > 35*60*60) {
			throw new ReplayException('Replays must finish in under an hour');
		}
	}
	
	protected function replayFormat() : string
	{
		$props = [
			// Header - should equal "\xF0" "KartReplay" "\x0F"
			'a12' . 'header',
			
			// Version numbers
			'c' . 'version',
			'c' . 'subversion',
			'v' . 'demoversion',
			
			// Checksum
			'a16' . 'checksum',
			
			// Is this just a "PLAY" constant?
			'a4' . 'playstring',
			
			// Game map number
			'v' . 'gamemap',
			
			// Map MD5
			'a16' . 'mapmd5',
			
			// Demo flags - This % 6 == 2 or replay is invalid
			'c' . 'demoflags',
			
			// Time/lap
			'V' . 'racetime',
			'V' . 'laptime',
			
			// Random seed. We don't care about this but it's in the way.
			'V' . 'UNUSED',
			
			// Player attributes
			'Z16' . 'name',
			'Z16' . 'skin',
			'Z16' . 'color',
			
			// Stats that aren't relevant for us
			'c5' . 'UNUSED',
			
			// Kart stats, to differentiate between characters with the same name and different stats.
			'c' . 'kartspeed',
			'c' . 'kartweight',
			
			// We don't care about the rest of this file.
		];
		
		return implode('/', $props);
	}
}
