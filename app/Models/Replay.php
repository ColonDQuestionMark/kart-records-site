<?php namespace App\Models;

use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Database\Eloquent\Model;

/**
 * An uploaded record attack replay.
 *
 * @property Map       $map       The specific variant of the course this replay was recorded on.
 * @property string    $name      The name of the player.
 * @property Character $character The character the player used.
 * @property string    $color     The color the player used on their character.
 * @property integer   $race_tics The total race time, in tics (1/35 seconds).
 * @property integer   $lap_tics  The best lap time, in tics (1/35 seconds).
 * @property string    $checksum  The MD5 hash of this replay.
 * @property string    $status    The admin-checked status of this replay.
 */
class Replay extends Model
{
	const STATUS_VERIFIED = 'Verified';
	const STATUS_MAJOR_SKIPS = 'Major Skips';
	const STATUS_HACKED = 'Hacked';
	const STATUS_HIDDEN = 'Hidden';
	
	const STATUSES = [
		self::STATUS_VERIFIED,
		self::STATUS_MAJOR_SKIPS,
		self::STATUS_HACKED,
		self::STATUS_HIDDEN,
	];
	
	const LEADERBOARD_STATUSES = [
		self::STATUS_VERIFIED,
		self::STATUS_MAJOR_SKIPS,
	];
	
	protected $fillable = ['name', 'color', 'race_tics', 'lap_tics', 'checksum', 'status'];
	
	public function course()
	{
		return $this->map->course();
	}
	
	public function map()
	{
		return $this->belongsTo(Map::class);
	}
	
	public function character()
	{
		return $this->belongsTo(Character::class);
	}
	
	public function getRaceTimeAttribute() : string
	{
		return $this->ticsToTime($this->race_tics);
	}
	
	public function getLapTimeAttribute() : string
	{
		return $this->ticsToTime($this->lap_tics);
	}
	
	protected function ticsToTime($tics) : string
	{
		$centiseconds = intval($tics * 100 / 35);
		$seconds = intval($tics/35);
		$minutes = intval($seconds / 60);
		
		return sprintf('%d\'%02d"%02d', $minutes, $seconds % 60, $centiseconds % 100);
	}
	
	public function getRaceRankAttribute() : string
	{
		return $this->rank('race_tics', 'time_leaderboard');
	}
	
	public function getLapRankAttribute() : string
	{
		return $this->rank('lap_tics', 'lap_leaderboard');
	}
	
	protected function rank($column, $leaderboard) : string
	{
		if (!$this->course) {
			return 'Unknown rank';
		}
		
		$betterTimes = $this->course->$leaderboard->filter(function (Replay $replay) use ($column) {
			return $replay->$column < $this->$column;
		})->count();
		
		$rank = $betterTimes + 1;
		
		if ($rank >= 10 && $rank < 20) {
			return $rank . 'th';
		}
		
		if ($rank % 10 == 1) {
			return $rank . 'st';
		}
		if ($rank % 10 == 2) {
			return $rank . 'nd';
		}
		if ($rank % 10 == 3) {
			return $rank . 'rd';
		}
		
		return $rank . 'th';
	}
	
	public function storeFile($data)
	{
		app(Filesystem::class)->put($this->getFilepath(), $data);
	}
	
	public function getFile() : string
	{
		return app(Filesystem::class)->get($this->getFilepath());
	}
	
	public function getFilepath() : string
	{
		return "replays/{$this->getKey()}";
	}
}
