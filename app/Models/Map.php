<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Map extends Model
{
	protected $fillable = ['checksum', 'map_number'];
	
	public function course()
	{
		return $this->belongsTo(Course::class);
	}
	
	public function getMapTagAttribute()
	{
		$num = $this->map_number;
		
		if ($num < 100) {
			return sprintf('MAP%02d', $num);
		}
		
		$num -= 100;
		
		$second = $num % 36;
		$first = intval($num / 36);
		
		$first = chr(ord('A') + $first);
		
		if ($second > 9) {
			$second = chr(ord('A') + $second - 10);
		}
		
		return 'MAP' . $first . $second;
	}
}
