<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Replay;
use Illuminate\Http\Request;

class ReplayController extends Controller
{
	public function index(Request $request)
	{
		$query = Replay::query();
	
		if ($request->get('unchecked')) {
			$query->whereNull('status');
		}
	
		return view('pages.admin.replay.index', [
			'replays' => $query->paginate(25)->appends($request->only('unchecked')), 
			'statuses' => Replay::STATUSES
		]);
	}
	
	public function update(Request $request, Replay $replay)
	{
		$replay->update($request->only('status'));
		
		return redirect()->back();
	}
}