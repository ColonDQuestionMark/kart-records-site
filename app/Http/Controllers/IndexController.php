<?php namespace App\Http\Controllers;

use App\Models\{Course, Replay, User};
use Carbon\Carbon;

class IndexController extends Controller
{
	public function index()
	{
		return view('pages.index', [
			'recent_replays'  => Replay::where(function ($query) {
				$query->whereIn('status', Replay::LEADERBOARD_STATUSES)->orWhereNull('status');
			})->has('map.course')->orderBy('created_at', 'desc')->take(10)->get(),
			
			'popular_courses' => Course::withReplayCount(Carbon::parse('1 week ago'))
				->orderBy('replay_count', 'desc')
				->take(5)
				->get(),
			
			'admins' => User::all(),
		]);
	}
}
