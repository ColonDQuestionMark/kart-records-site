<?php namespace App\Http\Controllers;

use App\Models\Course;
use Illuminate\Http\Request;

class CourseController extends Controller
{
	public function index()
	{
		return view('pages.course.index', ['courses' => Course::all()]);
	}
	
	public function show(Request $request, Course $course)
	{
		if (!($skips = $request->get('skips'))) {
			$course->hideMajorSkips();
		}
		
		return view('pages.course.show', ['course' => $course, 'skips' => $skips]);
	}
}
